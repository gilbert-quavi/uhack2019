import RPi.GPIO as GPIO
import time
from firebase import firebase

class Unit:
  def __init__(self,name,GPIO):
    self.name = name
    self.GPIO = GPIO

def checkStatus(ctr):
  status = firebase.get('/unitsPaymentStatus',units[ctr].name)
  if status == 1:
    GPIO.output(units[ctr].GPIO, GPIO.LOW)
  else:
    GPIO.output(units[ctr].GPIO, GPIO.HIGH)

GPIO.setmode(GPIO.BCM) # GPIO Numbers instead of board numbers
firebase = firebase.FirebaseApplication('https://nuxt-deploy-test.firebaseio.com/', None)


units = [
  Unit('unit-123',19),
  Unit('unit-124',13),
  Unit('unit-125',6)
]

# GPIO Assign
for ctr in range(len(units)):
  GPIO.setup(units[ctr].GPIO, GPIO.OUT) 

# Loop through units forever
while(True):
  for ctr in range(len(units)):
    checkStatus(ctr)
  time.sleep(1)