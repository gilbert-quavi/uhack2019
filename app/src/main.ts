import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import "./registerServiceWorker";
import BootstrapVue from 'bootstrap-vue';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';
import * as firebase from 'firebase';

var firebaseConfig = {
  apiKey: "AIzaSyDZAfI5qO9hfWfX5JdT9-bGUwLUYjXVXQg",
  authDomain: "nuxt-deploy-test.firebaseapp.com",
  databaseURL: "https://nuxt-deploy-test.firebaseio.com",
  projectId: "nuxt-deploy-test",
  storageBucket: "nuxt-deploy-test.appspot.com",
  messagingSenderId: "885338456926",
  appId: "1:885338456926:web:abaade9938e393e9"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

Vue.use(BootstrapVue)
Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
