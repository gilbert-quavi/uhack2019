export default  [
  {
    //
    amount: 5000,
    created: new Date().getTime(),
    due: new Date().getTime()+1296000,
    landlord: "kYscGq6FS4T7OdhVP1F7i5AW7dl1",
    tenant: "A8PhfqelMSdM6BFFUI3VDlV6MhW2",
    title: "Rent Fees",
    type: "RENT"
  },
  {
    //
    amount: 450,
    created: new Date().getTime(),
    due: new Date().getTime()+1296000,
    landlord: "kYscGq6FS4T7OdhVP1F7i5AW7dl1",
    tenant: "A8PhfqelMSdM6BFFUI3VDlV6MhW2",
    title: "Parking Fee",
    type: "MISC"
  },
  {
    //
    amount: 600,
    created: new Date().getTime(),
    due: new Date().getTime()+1296000,
    landlord: "kYscGq6FS4T7OdhVP1F7i5AW7dl1",
    tenant: "A8PhfqelMSdM6BFFUI3VDlV6MhW2",
    title: "House Repairs",
    type: "MISC",
  },
  {
    // Jarvs
    amount: 4200 ,
    created: new Date().getTime(),
    due: new Date().getTime()+1296000,
    landlord: "kYscGq6FS4T7OdhVP1F7i5AW7dl1",
    tenant: "A8PhfqelMSdM6BFFUI3VDlV6MhW2",
    title: "Electricity Fee",
    type: "ELECTRICITY",
  },
  {
    // RM
    amount: 5000,
    created: new Date().getTime(),
    due: new Date().getTime()+1296000,
    landlord: "kYscGq6FS4T7OdhVP1F7i5AW7dl1",
    tenant: "o0YwGal00bXmQeixMFO5qOtaTNr1",
    title: "Rent Fee",
    type: "RENT",
  },
  {
    //
    amount: 250,
    created: new Date().getTime(),
    due: new Date().getTime()+1296000,
    landlord: "kYscGq6FS4T7OdhVP1F7i5AW7dl1",
    tenant: "o0YwGal00bXmQeixMFO5qOtaTNr1",
    title: "House Repairs",
    type: "MISC"
  },
  {
    // RM
    amount: 1800,
    created: new Date().getTime(),
    due: new Date().getTime()+1296000,
    landlord: "kYscGq6FS4T7OdhVP1F7i5AW7dl1",
    tenant: "o0YwGal00bXmQeixMFO5qOtaTNr1",
    title: "Electricity Fee",
    type: "ELECTRICITY"
  },
  {
    // QUAVI
    amount: 5000,
    created: new Date().getTime(),
    due: new Date().getTime()+1296000,
    landlord: "kYscGq6FS4T7OdhVP1F7i5AW7dl1",
    tenant: "Ue8JI4fKrFUqYWojRuDr9SffL903",
    title: "Rent Fee",
    type: "RENT"
  },
  {
    //
    amount: 450,
    created: new Date().getTime(),
    due: new Date().getTime()+1296000,
    landlord: "kYscGq6FS4T7OdhVP1F7i5AW7dl1",
    tenant: "Ue8JI4fKrFUqYWojRuDr9SffL903",
    title: "Parking Fee",
    type: "MISC"
  },
  {
    //
    amount: 500,
    created: new Date().getTime(),
    due: new Date().getTime()+1296000,
    landlord: "kYscGq6FS4T7OdhVP1F7i5AW7dl1",
    tenant: "Ue8JI4fKrFUqYWojRuDr9SffL903",
    title: "Additional Amenities",
    type: "MISC"
  },
  {
    //
    amount: 7200,
    created: new Date().getTime(),
    due: new Date().getTime()+1296000,
    landlord: "kYscGq6FS4T7OdhVP1F7i5AW7dl1",
    tenant: "Ue8JI4fKrFUqYWojRuDr9SffL903",
    title: "Electricity Fee",
    type: "ELECTRICITY"
  },
];