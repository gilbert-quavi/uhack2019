export default {
  headers: {
    headers: {
      'Content-Type': 'application/json',
      'Authorization': '<string>',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE,PATCH,OPTIONS',
      'Access-Control-Allow-Headers': 'Access-Control-Allow-Origin'
    }
  },
}