import { Request, Response } from 'express';
import { checkSearchParams } from '../../middleware/checks';

import controller from './controller'

const root = '/api/v1/llpayments'

export default [
  {
    // accepts a payment request
    // will make payment using union bank api
    path: root + '/accept',
    method: 'post',
    handler: [
      async (req: Request, res: Response) => {
        const paymentRequestResult = await controller.requestPayment(req.body)
        res.status(200).send(paymentRequestResult);
      }
    ]
  },
  {
    path: root + '/test0',
    method: 'get',
    handler: [
      // checkSearchParams,
      async ({ query }: Request, res: Response) => {
        res.status(200).send({
          value: 'Test in payments endpoint is working'
        });
      }
    ]
  },
  {
    path: root + '/test1',
    method: 'post',
    handler: [
      async ({ body }: Request, res: Response) => {
        res.status(200).send({
          value: {
            body
          }
        });
      }
    ]
  },
  {
    path: root + '/test2/:id',
    method: 'get',
    handler: [
      async (req: Request, res: Response) => {
        res.status(200).send({
          value: req.params.id
        });
      }
    ]
  },
  {
    path: root + '/token',
    method: 'get',
    handler: [
      async (req: Request, res: Response) => {
        controller.testToken().then(r => {
          res.status(200).send(r);
        })
      }
    ]
  },
];

