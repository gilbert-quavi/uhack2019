import config from './config'
import axios from 'axios'
import { rejects } from 'assert'
import firebase from 'firebase-admin'

// import models
import { LLPaymentRequest } from '~shared/landlord/LLPaymentRequest'
import { LLPaymentResult } from '~shared/landlord/LLPaymentResult';

// status can either be 1 or 0
const updatePaymentStatus = (unitId: string, status: number) => {
  return new Promise((resolve, reject) => {
    firebase.database()
      .ref('unitsPaymentStatus')
      .child(unitId).set(status)
      .then(res => {
        resolve(res)
      }).catch(err => {
        reject(err)
      })
  })
}

const requestPayment = (paymentRequest: LLPaymentRequest) => {
  // 1. Make the payment using ubank api 
  // 2. If payment is successful, post the payment to database
  // 3. Update payment status from the database
  updatePaymentStatus(paymentRequest.unitId, 1)
  return new Promise((resolve, reject) => {
    resolve({
      // 4. Return payment result here
      key: 'TEST VALUE',
      request: paymentRequest
    })
  })
}


const testToken = () => {
  return new Promise((resolve, reject) => {
    axios({
      method: "post",
      url:
        "https://api-uat.unionbankph.com/partners/sb/convergent/v1/oauth2/token",
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        "User-Agent": "PostmanRuntime/7.17.1",
        "Accept": "*/*",
        "Cache-Control": "no-cache",
        "Postman-Token": "6aa87686-b2cf-4619-a16e-c71072a2eb49,a4d53773-e43b-4dcd-b031-7b1f2d12a316",
        "Host": "api-uat.unionbankph.com",
        "Accept-Encoding": "gzip, deflate",
        "Content-Length": "510",
        "Connection": "keep-alive",
        "cache-control": "no-cache"
      },
      data: {
        "grant_type": "authorization_code",
        "client_id": "e527be1e-4497-4f73-a082-403bf1e25b0d",
        "code": "AAIgGKMyhCkeyyKtJCB02iVloTWCmdTYu4vNimRPTix1Klx4yPLO5kud8rcevBn09JgvKjji-oCkJCOt8ilmSwFo3ZB8OFLvaZCAmJzUH4QieZdNmXpOXmdJJEFGG-4Qk2mvUKU2siPtMMr8M9vmriQMe7gUcjXLscI712hHmntB8j7QVFZi1m2ag9Rfu8DogXzu9KzpTa_aLFYo22xSDN8WefDD1OcL3nsl5GBduGi6a7YDPEa9gR4aWkPMO5HBbb8GR3WVZS0oBkrEYmIZcWHMmWJj0kqNkzGWusa7NIejaUwlTIn82N4zkJTSfBuRVOMS-ieKc6BaCn_oz3NOaoERdZegr-GIlLFYRN2IVy1pKA",
        "redirect_uri": "http://192.168.43.75:8080/confirmation"
      }
    }).then(res => {
      resolve(res)
    }).catch(err => {
      resolve(err)
    })
  })
}

export default {
  requestPayment,
  testToken
}