import * as firebase from 'firebase-admin'

export default {
  start() {
    console.log('STARTING TEST')

    const serviceAccount = require('../../firebaseconfig.json')
    firebase.initializeApp({
      credential: firebase.credential.cert(serviceAccount),
      databaseURL: 'https://nuxt-deploy-test.firebaseio.com'
    })

    firebase.firestore().collection('testCollection').doc('testDoc').set({
      testValue: 'HELLO WORLD',
      date: new Date().toString()
    })
  }
}