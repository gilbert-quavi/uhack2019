export interface LLPaymentRequest {
  // timestamp in long format 
  date: number
  amount: number
  // the uid of the user paying
  tenantId: string
  // the id of the unit
  unitId: string
  token?: string
}
// generate