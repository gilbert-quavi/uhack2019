export interface LLUser {
  name?: string
  email?: string
  uid?: string
  // TENANT, LANDLORD
  
  userType?: string
  lastLogin?: number
}
// generate