export interface LLBill {
  // RENT, MISC or ELECTRICITY
  type: string
  due: number
  created: number
  title: string
  details: string
  amount: number
}
// generate