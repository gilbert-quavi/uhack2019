import { LLBill } from "./LLBill";
import { LLPayment } from "./LLPayment";

export interface LLUnit {
  // id of the tenant
  tenant: string
  // id of the admin
  landlord: string
  
  // monthly cost of the unit
  cost: number

  bills: LLBill[]
  payments: LLPayment[]
}
// generate